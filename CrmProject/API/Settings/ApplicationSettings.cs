﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Settings
{
    public class ApplicationSettings
    {
        public string JWT_Key { get; set; }
    }
}
