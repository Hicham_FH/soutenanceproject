using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Settings;
using AutoMapper;
using Core.Entities;
using Core.Helpers;
using Core.Interfaces;
using Core.Service;
using Infrastructure.Data;
using Infrastructure.Repositories;
using Infrastructure.ServiceImpl;
using Infrastructure.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace API
{
    public class Startup
    {
        //readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));


            services.AddControllers();

            services.AddCors();


            services.AddAutoMapper(typeof(AutoMapperProfile));


            //========== UNIT OF WORK ========//

            services.AddScoped(typeof(IUnitOfWork<>) , typeof(UnitOfWork<>));

            //======= Authenticate Respository ====//

            services.AddScoped<IAuthenticateRepository, AuthenticateRepository>();

            //======= Dashboard Respository ====//

            services.AddScoped<IDashboardRepository, DashboardRepository>();

            //======= Profile Service ====//

            services.AddScoped<IProfileService, ProfileService>();

            //======= Pays Service ====//

            services.AddScoped<IPaysService, PaysService>();

            //======= Ville Service ====//

            services.AddScoped<IVilleService, VilleService>();

            //==== DB Context ====//

            services.AddDbContext<DataContext>(option =>
            {
                option.UseSqlServer(Configuration.GetConnectionString("DataConnection"));
            });

            //==== Identity ===//

            services.AddIdentity<ApplicationUser, ApplicationRole>().AddEntityFrameworkStores<DataContext>();

            //========Add JWT======//

            var key = System.Text.Encoding.UTF8.GetBytes(Configuration["ApplicationSettings:JWT_Key"].ToString());

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

            //=====Add Cors =========//

            
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

          

            app.UseRouting();

            app.UseCors(options => options.WithOrigins("http://localhost:4200")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                );

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
