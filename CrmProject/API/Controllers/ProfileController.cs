﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _profileService;

        public ProfileController(IProfileService profileService)
        {
            this._profileService = profileService;
        }

        [HttpGet]
        [Authorize]
        public async Task<Object> GetProfile()
        {
            int UserID = int.Parse(User.Claims.FirstOrDefault(u => u.Type == "UserID").Value.ToString());

            var user = await _profileService.GetUserInfoById(UserID);

            return user;

        }

    }
}