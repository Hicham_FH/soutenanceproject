﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VilleController : ControllerBase
    {
        private readonly IUnitOfWork<Ville> _Entity;

        private readonly IVilleService _service;

        public VilleController(IUnitOfWork<Ville> Entity , IVilleService service)
        {
            this._Entity = Entity;
            _service = service;
        }

        //======== Get Method =====//

        [HttpGet]
        public IEnumerable<Object> Get()
        {
            //return await _Entity.Entity.GetAll();
            return _service.getVilleData();
        }

        //======== Get By ID Method =====//

        [HttpGet("{id}")]
        public async Task<ActionResult<Ville>> GetPaysByID(int id)
        {
            var ville = await _Entity.Entity.GetById(id);

            if (ville == null)
            {
                return NotFound();
            }

            return ville;
        }

        //======== Post Method =====//

        [HttpPost]
        public async Task<ActionResult<Ville>> Post(Ville ville)
        {
            await _Entity.Entity.Post(ville);
            return CreatedAtAction("Get", new { id = ville.Id }, ville);
        }

        //======= Delete Method ====//

        [HttpDelete("{id}")]
        public async Task<ActionResult<Ville>> DeletePays(int id)
        {
            var ville = await _Entity.Entity.GetById(id);

            if (ville == null)
            {
                return NotFound();
            }

            await _Entity.Entity.Delete(ville);

            return ville;
        }

        //======= PUT Method =====//

        [HttpPut("{id}")]
        public IActionResult PutPays(int id, Ville ville)
        {
            if (id != ville.Id)
            {
                return BadRequest("Failed ID");

            }


            _Entity.Entity.Put(ville);
            try
            {
                _Entity.Save();
            }
            catch (System.Exception ex)
            {

                return BadRequest(ex.Message);

            }

            return Ok("Update Success");
        }
    }
}