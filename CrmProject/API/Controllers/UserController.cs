﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Core.Dtos;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUnitOfWork<ApplicationUser> _Entity;
        private readonly IMapper _mapper;
        private readonly IAuthenticateRepository _repository;

        public UserController(IUnitOfWork<ApplicationUser> Entity, IMapper mapper, IAuthenticateRepository repository)
        {
            _mapper = mapper;
            _repository = repository;
            this._Entity = Entity;
        }

        //======== Get Method =====//

        [HttpGet]
        public async Task<IEnumerable<ApplicationUser>> Get()
        {
            return await _Entity.Entity.GetAll();
        }

        //======== Get By ID Method =====//

        [HttpGet("{id}")]
        public async Task<ActionResult<ApplicationUser>> GetPaysByID(int id)
        {
            var user = await _Entity.Entity.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        //======== Post Method =====//

        [HttpPost]
        public async Task<ActionResult<ApplicationUser>> Post(RegisterDto model)
        {
            // await _Entity.Entity.Post(user);
            try
            {
                if (model == null) return NotFound();

                if (ModelState.IsValid)
                {
                    model.Username = model.Username.ToLower();

                    if (await _repository.UsernameExist(model.Username))
                        return BadRequest("Username Existe Déja");

                    var role = model.Role;

                    if (await _repository.RoleExist(role))
                        return BadRequest("Role Not Found");

                    var user = _mapper.Map<ApplicationUser>(model);

                    var userCreated = await _repository.Register(user, model.Password, role);

                    return StatusCode(201, new
                    {
                        username = userCreated.UserName,
                        email = userCreated.Email,
                        fullname = userCreated.FullName,
                        role = model.Role,
                        phoneNumber = userCreated.PhoneNumber,
                        lastName = userCreated.LastName,
                        firstName = userCreated.FirstName,
                        description = userCreated.Description,
                        imageUrl = userCreated.UrlImage
                    });
                }
                return StatusCode(StatusCodes.Status400BadRequest);

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        //======= Delete Method ====//

        [HttpDelete("{id}")]
        public async Task<ActionResult<ApplicationUser>> DeleteUser(int id)
        {
            var user = await _Entity.Entity.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            await _Entity.Entity.Delete(user);

            return user;
        }

        //======= PUT Method =====//

        [HttpPut("{id}")]
        public IActionResult PutUser(int id, ApplicationUser user)
        {
            if (id != user.Id)
            {
                return BadRequest("Failed ID");

            }


            _Entity.Entity.Put(user);
            try
            {
                _Entity.Save();
            }
            catch (System.Exception ex)
            {

                return BadRequest(ex.Message);

            }

            return Ok("Update Success");
        }
    }
}