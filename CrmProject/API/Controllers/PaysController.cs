﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaysController : ControllerBase
    {
        private readonly IUnitOfWork<Pays> _Entity;
        private readonly IPaysService _service;

        public PaysController(IUnitOfWork<Pays> Entity , IPaysService service)
        {
            this._Entity = Entity;
            _service = service;
        }

        //======== Get Method =====//
        /*
                [HttpGet]
                public async Task<IEnumerable<Pays>> Get()
                {
                    return await _Entity.Entity.GetAll();
                }
                */
        [HttpGet]
        public IEnumerable<Object> Get()
        {
            return _service.getPaysData();
        }

        //======== Get By ID Method =====//

        [HttpGet("{id}")]
        public async Task<ActionResult<Pays>> GetPaysByID(int id)
        {
            var pays = await _Entity.Entity.GetById(id);

            if (pays == null)
            {
                return NotFound();
            }

            return pays;
        }

        //======== Post Method =====//

        [HttpPost]
        public async Task<ActionResult<Pays>> Post(Pays pays)
        {
            await _Entity.Entity.Post(pays);
            return CreatedAtAction("Get", new { id = pays.Id }, pays);
        }

        //======= Delete Method ====//

        [HttpDelete("{id}")]
        public async Task<ActionResult<Pays>> DeletePays(int id)
        {
            var pays = await _Entity.Entity.GetById(id);

            if (pays == null)
            {
                return NotFound();
            }

            await _Entity.Entity.Delete(pays);

            return pays;
        }

        //======= PUT Method =====//

        [HttpPut("{id}")]
        public IActionResult PutPays(int id, Pays pays)
        {
            if (id != pays.Id)
            {
                return BadRequest("Failed ID");

            }

           
            _Entity.Entity.Put(pays);
            try
            {
                 _Entity.Save();
            }
            catch (System.Exception ex)
            {

                return BadRequest(ex.Message);
                
            }

            return Ok("Update Success");
        }




    }
}