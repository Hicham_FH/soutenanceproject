﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private IDashboardRepository _repository;

        public DashboardController(IDashboardRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Authorize]
        [Route("GetUserByID")]
        public async Task<Object> GetUserInfo()
        {
            int userID = int.Parse(User.Claims.FirstOrDefault(u => u.Type == "UserID").Value.ToString());

            var user = await _repository.GetUserInfoById(userID);

            return user;
        }

        [HttpGet]
        [Authorize]
        [Route("getAllRole")]
        public async Task<IEnumerable<ApplicationRole>> getAllRole()
        {
            return await _repository.getAllRole();
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("GetAllUsers")]
        public IEnumerable<Object> getAllUsers()
        {
            return _repository.getAllUsers();
        }


        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        [Route("deleteUser")]
        public async Task<ActionResult<ApplicationUser>> deleteUser(int userID)
        {
            var user = await _repository.getUser(userID);

            if (user == null)
            {
                return BadRequest("rak ghalet");
            }

            await _repository.deleteUser(user);

            return user;
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("numberCommercial")]
        public int CommercialCount()
        {
            int user = _repository.getCommercialNumber();

            return user;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("numberClient")]
        public int ClientCount()
        {
            int client = _repository.getClientNumber();

            return client;
        }
    }
}