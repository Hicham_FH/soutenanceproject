﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IUnitOfWork<Client> _Entity;

        public ClientController(IUnitOfWork<Client> Entity)
        {
            this._Entity = Entity;
        }

        //======== Get Method =====//

        [HttpGet]
        public async Task<IEnumerable<Client>> Get()
        {
            return await _Entity.Entity.GetAll();
        }

        //======== Get By ID Method =====//

        [HttpGet("{id}")]
        public async Task<ActionResult<Client>> GetPaysByID(int id)
        {
            var client = await _Entity.Entity.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }

        //======== Post Method =====//

        [HttpPost]
        public async Task<ActionResult<Client>> Post(Client client)
        {
            await _Entity.Entity.Post(client);
            return CreatedAtAction("Get", new { id = client.Id }, client);
        }

        //======= Delete Method ====//

        [HttpDelete("{id}")]
        public async Task<ActionResult<Client>> DeletePays(int id)
        {
            var client = await _Entity.Entity.GetById(id);

            if (client == null)
            {
                return NotFound();
            }

            await _Entity.Entity.Delete(client);

            return client;
        }

        //======= PUT Method =====//

        [HttpPut("{id}")]
        public IActionResult PutPays(int id, Client client)
        {
            if (id != client.Id)
            {
                return BadRequest("Failed ID");

            }


            _Entity.Entity.Put(client);
            try
            {
                _Entity.Save();
            }
            catch (System.Exception ex)
            {

                return BadRequest(ex.Message);

            }

            return Ok("Update Success");
        }
    }
}