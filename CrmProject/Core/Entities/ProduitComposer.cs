﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class ProduitComposer
    {
        [Key]
        public int Id { get; set; }

        public int Qte { get; set; }

        public Produit Produit { get; set; }

        [ForeignKey("Produit")]
        public int ProduitId { get; set; }


    }
}
