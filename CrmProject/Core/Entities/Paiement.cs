﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Paiement
    {
        [Key]
        public int Id { get; set; }

        public DateTime DatePaiement { get; set; }

        public float Montant { get; set; }

        public DateTime DateEcheance { get; set; }

        public Boolean Timbre { get; set; }


        //======== Document Objet =====//

        public virtual Document Document { get; set; }

        [ForeignKey("Document")]
        public int DocumentId { get; set; }

        //==== Type Paiement Objet =====//

        public virtual TypePaiement TypePaiement { get; set; }

        [ForeignKey("TypePaiement")]
        public int TypePaiementId { get; set; }
    }
}
