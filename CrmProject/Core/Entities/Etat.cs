﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Etat
    {
        [Key]
        public int Id { get; set; }

        public String Type { get; set; }


        //====== Liste Des Etat Des Clients ====//

        public virtual List<EtatClient> EtatClients { get; set; }

    }
}
