﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Pays
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        public String Devise { get; set; }

        //======== List Des Ville =====//

        public virtual List<Ville> Villes { get; set; }
    }
}
