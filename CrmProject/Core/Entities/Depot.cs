﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Depot
    {
        [Key]
        public int Id { get; set; }

        public float QteMax { get; set; }

        public String Adresse { get; set; }

        public String Lattitude { get; set; }

        public String Longtitude { get; set; }

        //======== Ville Object ======//

        public virtual Ville Ville { get; set; }

        [ForeignKey("Ville")]
        public int VilleId { get; set; }


        //====== Lot Object ====//

        public virtual Lot Lot { get; set; }

        [ForeignKey("Lot")]
        public int LotId { get; set; }


    }
}
