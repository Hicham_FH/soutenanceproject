﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Ville
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        //======== Pays objet ========//

        public virtual Pays Pays { get; set; }

        [ForeignKey("Pays")]
        public int PaysId { get; set; }

        //=========== List Des Client ======//

        public virtual List<Client> Clients { get; set; }

        //========= Liste Des Dépot =====//

        public virtual List<Depot> Depots { get; set; }
    }
}
