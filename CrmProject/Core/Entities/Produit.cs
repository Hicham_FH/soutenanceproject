﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Produit
    {
        [Key]
        public int Id { get; set; }

        public String Description { get; set; }

        public String Fabricant { get; set; }

        public String QR { get; set; }


        //====== Type Produit Object ====//

        public virtual TypeProduit TypeProduit { get; set; }

        [ForeignKey("TypeProduit")]
        public int TypeProduitId { get; set; }


        //========= Liste Des Lots =====//

        public virtual List<Lot> Lots { get; set; }

        //========= Liste Des Attributs =====//

        public virtual List<Attribut> Attributs { get; set; }

        //========= Liste Des Brouillons =====//

        public virtual List<Brouillon> Brouillons { get; set; }

        //========= Liste Des Promotions =====//

        public virtual List<Promotion> Promotions { get; set; }

        //======= Liste Des Produit Saison ===//

        public List<ProduitSaison> ProduitSaisons { get; set; }


        //======= Liste Des Sous Produit Parent Et Enfant ======//

        [InverseProperty("ProduitParent")]
        public virtual List<SousProduits> EnTantQueParent { get; set; }

        [InverseProperty("ProduitEnfant")]
        public virtual List<SousProduits> EnTantQueEnfant { get; set; }

        //========== Liste Des Produit Composer ============//

        public virtual List<ProduitComposer> ProduitComposers { get; set; }





    }
}
