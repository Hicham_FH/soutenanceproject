﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class ClientInteret
    {
        public int Id { get; set; }

        //======== Client Objet ========//
        public virtual Client Client { get; set; }

        public int ClientId { get; set; }

        //====== Etat Interet Objet ======//

        public virtual EtatInteret EtatInteret { get; set; }

        public int EtatInteretId { get; set; }

    }
}
