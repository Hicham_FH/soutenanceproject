﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class RaisonReclamation
    {
        [Key]
        public int Id { get; set; }

        public Raison Raison { get; set; }

        public int RaisonId { get; set; }

        public Reclamation Reclamation { get; set; }

        public int ReclamationId { get; set; }



    }
}
