﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class ApplicationUser : IdentityUser<int>
    {
        public String FullName { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public Ville Ville { get; set; }

        public String Adress { get; set; }

        public String ZipCode { get; set; }

        public String Description { get; set; }

        public String Grade { get; set; }

        public String UrlImage { get; set; }

        //======= Liste De Description Action =====//
        public virtual List<DescriptionAction> DescriptionActions { get; set; }

        


    }
}
