﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class LigneDocument
    {
        public int Id { get; set; }

        public DateTime DateAction { get; set; }

        public float PU { get; set; }

        public String Action { get; set; }

        public int NbrImpression { get; set; }

        public DateTime DateImpression { get; set; }

        public String CodeLigne { get; set; }

        public float Remise { get; set; }

        //======== Document Objet =====//

        public virtual Document Document { get; set; }

        [ForeignKey("Document")]
        public int DocumentId { get; set; }

        //====== Lot Object ====//

        public virtual Lot Lot { get; set; }

        [ForeignKey("Lot")]
        public int LotId { get; set; }
    }
}
