﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class AttributProduit
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        public String Type { get; set; }

        //====== Object TypeProduit =======//

        public virtual TypeProduit TypeProduit { get; set; }

        [ForeignKey("TypeProduit")]
        public int TypeProduitId { get; set; }


    }
}
