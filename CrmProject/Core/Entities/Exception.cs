﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Exception
    {
        [Key]
        public int Id { get; set; }

        public String Description { get; set; }

        //====== Client Object =======//

        public virtual Client Client { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }
    }
}
