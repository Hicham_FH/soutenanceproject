﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Commentaire
    {
        [Key]
        public int Id { get; set; }

        public String Text { get; set; }

        public DateTime DateCreation { get; set; }

        //========Client Object =========//

        public virtual Client Client { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }



    }
}
