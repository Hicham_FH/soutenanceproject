﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Client
    {
        [Key]
        public int Id { get; set; }

        public String Nom { get; set; }

        public String Prenom { get; set; }

        public DateTime DateNaissance { get; set; }

        public String CIN { get; set; }

        public String Adresse { get; set; }

        public String Tel1 { get; set; }

        public String Tel2 { get; set; }

        public String Email { get; set; }

        public String Sexe { get; set; }

        public SByte NombreEnfant { get; set; }

        public String Fonction { get; set; }

        public DateTime DateAjout { get; set; }

        public DateTime DateModif { get; set; }

        public float Point { get; set; }

        public float CA { get; set; }


        //========Objet Ville ===========//

        public virtual Ville Ville { get; set; }

        [ForeignKey("Ville")]
        public int VilleId { get; set; }




        //======= List Des Promess =====//

        public virtual List<Promesse> Promesses { get; set; }

        //======= List Des Commentaire ====//

        public virtual List<Commentaire> Commentaires { get; set; }

        //======= List Des Communication ======//

        public virtual List<Communication> Communications { get; set; }

        //======= List Des Enfants =============//

        public virtual List<Enfant> Enfants { get; set; }

        //======= List Des Exceptions =============//

        public virtual List<Exception> Exceptions { get; set; }


        //========= List Des Interet ========//

        public virtual List<ClientInteret> ClientInterets { get; set; }

        //====== Liste Des Etat Des Clients ====//

        public virtual List<EtatClient> EtatClients { get; set; }

        //====== Liste Des Documents ====//

        public virtual List<Document> Documents { get; set; }












    }
}
