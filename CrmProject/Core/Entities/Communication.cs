﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Communication
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateCreation { get; set; }

        public String Type { get; set; }

        public String Objectif { get; set; }

        public String Commentaire { get; set; }

        //======= Client Object =========//

        public virtual Client Client { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }


    }
}
