﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class SousProduits
    {
        [Key]
        public int Id { get; set; }

        public Produit ProduitParent { get; set; }



        public Produit ProduitEnfant { get; set; }



    }
}
