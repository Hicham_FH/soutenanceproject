﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Lot
    {
        [Key]
        public int Id { get; set; }

        public int QteRestante { get; set; }

        public float PrixAchat { get; set; }

        public float PrixVente { get; set; }

        public DateTime DateExpiration { get; set; }

        //========== Produit Object ====//

        public Produit Produit { get; set; }

        [ForeignKey("Produit")]
        public int ProduitId { get; set; }


        //========= Liste Des Dépot =====//

        public virtual List<Depot> Depots { get; set; }

        //========Liste Des Ligne Document =====//

        public virtual List<LigneDocument> LigneDocuments { get; set; }

        //======= Liste Depot Lot =======//

        public virtual List<DepotLot> DepotLots { get; set; }




    }
}
