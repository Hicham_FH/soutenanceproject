﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Document
    {
        [Key]
        public int Id { get; set; }

        public float Remise { get; set; }

        public float Avance { get; set; }

        public float TotalAPayer { get; set; }

        public float TotalRegle { get; set; }

        public Boolean IsUrgence { get; set; }

        public Boolean IsRetard { get; set; }

        public DateTime DateLivraisonPrevu { get; set; }

        public String Etape { get; set; }



        //======= Client Object =========//

        public virtual Client Client { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }


        //======== Liste Des Reclamation ====//

        public virtual List<Reclamation> Reclamations { get; set; }

        //======= Liste Des Paiement ====//

        public virtual List<Paiement> Paiements { get; set; }

        //======= Liste Des Ligne De Document ===//

        public virtual List<LigneDocument> ligneDocuments { get; set; }

        //======= Liste De Description Action =====//

        public virtual List<DescriptionAction> DescriptionActions { get; set; }


    }
}
