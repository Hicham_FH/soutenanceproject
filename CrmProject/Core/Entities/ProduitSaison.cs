﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class ProduitSaison
    {
        [Key]
        public int Id { get; set; }

        public int DureeVie { get; set; }

        public float MargeRemise { get; set; }

        public int NbrJourAvantAlerte { get; set; }

        //========== Produit Object ====//

        public Produit Produit { get; set; }

        [ForeignKey("Produit")]
        public int ProduitId { get; set; }

        //========== Produit Object ====//

        public virtual Saison Saison { get; set; }

        [ForeignKey("Saison")]
        public int SaisonId { get; set; }
    }
}
