﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class TypeProduit
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        //======= Liste Des Produits ===//

        public virtual List<Produit> Produits { get; set; }

        //====== Liste Des Attribut Produit =======//

        public virtual List<AttributProduit> AttributProduits { get; set; }


    }
}
