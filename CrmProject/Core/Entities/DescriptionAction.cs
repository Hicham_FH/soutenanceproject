﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class DescriptionAction
    {
        public int Id { get; set; }

        public String Description { get; set; }

        public DateTime DateCreation { get; set; }


        [ForeignKey("ApplicationUser")]
        public int ApplcicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }  

        [ForeignKey("Document")]
        public int DocumentId { get; set; }

        public Document Document { get; set; }

    }
}
