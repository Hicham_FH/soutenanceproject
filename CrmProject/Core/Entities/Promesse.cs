﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Promesse
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateCreation { get; set; }

        public DateTime DateDebut { get; set; }

        public DateTime DateFin { get; set; }

        public float Pourcentage { get; set; }

        public float CAPotentiel { get; set; }

        public String Commentaire { get; set; }

        //==========Client Object=======//

        public virtual Client Client { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }

    }
}
