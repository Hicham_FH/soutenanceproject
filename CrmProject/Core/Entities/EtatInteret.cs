﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class EtatInteret
    {
        [Key]
        public int Id { get; set; }

        public String DescriptionKeywords { get; set; }

        //========= List Des Interet ========//

        public virtual List<ClientInteret> ClientInterets { get; set; }

    }
}
