﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Niveau
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        //======= Colonne Object ===//

        public virtual Colonne Colonne { get; set; }

        [ForeignKey("Colonne")]
        public int ColonneId { get; set; }

        //======= Rayon Object ===//

        public virtual Rayon Rayon { get; set; }

        [ForeignKey("Rayon")]
        public int RayonId { get; set; }

        




    }
}
