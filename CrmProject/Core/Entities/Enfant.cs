﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Enfant
    {
        [Key]
        public int Id { get; set; }

        public String Nom { get; set; }

        public String Prenom { get; set; }

        public DateTime DateNaissance { get; set; }


        //========= Client Object =====//

        public virtual Client Client { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }



    }
}
