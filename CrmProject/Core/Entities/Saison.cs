﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Saison
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        public DateTime DateDebut { get; set; }

        public DateTime DateFin { get; set; }


        //======= Liste Des Produit Saison ===//

        public List<ProduitSaison> ProduitSaisons { get; set; }


    }
}
