﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class EtatClient
    {
        public int Id { get; set; }

        public DateTime DateEtatDebut { get; set; }

        public DateTime DateEtatFin { get; set; }


        //======= Client Object =========//

        public virtual Client Client { get; set; }

        public int ClientId { get; set; }

        //======= Client Object =========//

        public virtual Etat Etat { get; set; }

        public int EtatId { get; set; }
    }
}
