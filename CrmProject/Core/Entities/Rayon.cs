﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Rayon
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        //======= Liste Depot Lot =======//

        public virtual List<Niveau> Niveaus { get; set; }

    }
}
