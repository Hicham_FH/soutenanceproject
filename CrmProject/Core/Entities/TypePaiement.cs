﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class TypePaiement
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }


        //======= Liste Des Paiement ====//

        public virtual List<Paiement> Paiements { get; set; }
    }
}
