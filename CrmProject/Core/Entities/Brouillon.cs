﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Brouillon
    {
        [Key]
        public int Id { get; set; }

        public int Manque { get; set; }

        public DateTime DateCreation { get; set; }

        //========== Produit Object ====//

        public virtual Produit Produit { get; set; }

        [ForeignKey("Produit")]
        public int ProduitId { get; set; }
    }
}
