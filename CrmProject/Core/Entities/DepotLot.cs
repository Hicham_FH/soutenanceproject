﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class DepotLot
    {
        public int Id { get; set; }

        public DateTime DateDepot { get; set; }

        //====== Lot Object ====//

        public virtual Lot Lot { get; set; }

        [ForeignKey("Lot")]
        public int LotId { get; set; }

        //======= Colonne Object ===//

        public virtual Colonne Colonne { get; set; }

        [ForeignKey("Colonne")]
        public int ColonneId { get; set; }

    }
}
