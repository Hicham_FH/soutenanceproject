﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Attribut
    {
        [Key]
        public int Id { get; set; }

        public String Libelle { get; set; }

        public String Valeur { get; set; }

        //========== Produit Object ====//

        public Produit Produit { get; set; }

        [ForeignKey("Produit")]
        public int ProduitId { get; set; }
    }
}
