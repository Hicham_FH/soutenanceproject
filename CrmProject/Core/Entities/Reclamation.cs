﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Reclamation
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateReclamation { get; set; }

        public String Description { get; set; }

        public String Niveau { get; set; }


        //======== Document Objet =====//

        public virtual Document Document { get; set; }

        [ForeignKey("Document")]
        public int DocumentId { get; set; }


        //=========Raison ======//
    }
}
