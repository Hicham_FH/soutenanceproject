﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Promotion
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateDebut { get; set; }

        public DateTime DateFin { get; set; }

        public float Pourcentage { get; set; }

        public float Reduction { get; set; }

        public DateTime DateCreation { get; set; }

        //========== Produit Object ====//

        public virtual Produit Produit { get; set; }

        [ForeignKey("Produit")]
        public int ProduitId { get; set; }
    }
}
