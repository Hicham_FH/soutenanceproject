﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class RegisterDto
    {
        public String Username { get; set; }
        public String FullName { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }

        public String PhoneNumber { get; set; }

        public String Role { get; set; }


        public String FirstName { get; set; }

        public String LastName { get; set; }


        public String Adress { get; set; }

        public String ZipCode { get; set; }

        public String Description { get; set; }

        public String Grade { get; set; }

        public String UrlImage { get; set; }
    }
}
