﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dtos
{
    public class LoginDto
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
