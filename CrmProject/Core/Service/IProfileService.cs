﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Service
{
    public interface IProfileService
    {
        //==========Get User By ID ============//
        Task<Object> GetUserInfoById(int userID);
    }
}
