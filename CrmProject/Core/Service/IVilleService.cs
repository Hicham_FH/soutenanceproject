﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Service
{
    public interface IVilleService
    {
        IQueryable<Object> getVilleData();
    }
}
