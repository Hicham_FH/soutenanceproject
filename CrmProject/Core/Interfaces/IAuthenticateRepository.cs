﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IAuthenticateRepository 
    {
        //=======Login Method ===============//
        Task<ApplicationUser> Login(String Username, String Password);

        //========Register Method ==========//
        Task<ApplicationUser> Register(ApplicationUser user, String Password, string role);

        //========Username Exist ==========//
        Task<Boolean> UsernameExist(String Username);

        //=======Role Exist ===========//

        Task<Boolean> RoleExist(String role);

        //===========Get Role =========//

        Task<IList<string>> GetRolesAsync(ApplicationUser user);
    }
}
