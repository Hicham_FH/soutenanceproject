﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IDashboardRepository
    {
        //==========Get User By ID ============//
        Task<Object> GetUserInfoById(int userID);

        //==========Get All Users ===========//

        IEnumerable<Object> getAllUsers();

        //======== Get All Role ==========//

        Task<IEnumerable<ApplicationRole>> getAllRole();

        //======= Delete Utilisateur ====//

        Task<ApplicationUser> deleteUser(ApplicationUser user);

        //========= Get User =========//
        Task<ApplicationUser> getUser(int userID);

        //======== Get Commerciale Count =======//

       int getCommercialNumber();

        //======== Get Client Count ======//

        int getClientNumber();
    }
}
