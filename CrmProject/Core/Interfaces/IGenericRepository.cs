﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        //========= Get All =======//

        Task<IEnumerable<T>> GetAll();

        //========= Get By ID =====//

        Task<T> GetById(int id);

        //======== Post Data =======//

        Task<T> Post(T entity);

        //======= Delete Data ======//

        Task<T> Delete(T entity);

        //======== PUT Data =========//

        void Put(T entity);

        


    }
}
