﻿using Core.Service;
using Infrastructure.Data;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.ServiceImpl
{
    public class PaysService : IPaysService
    {
      //  private readonly UserManager<ApplicationUser> _userManager;
        private readonly DataContext _db;
        public PaysService( DataContext db)
        {
            _db = db;
        }

        public IQueryable<Object> getPaysData()
        {
            var req = from p in _db.Pays
                     
                      select new
                      {
                          id = p.Id,
                          libelle = p.Libelle,
                          devise = p.Devise,
                          nbrVille = p.Villes.Count()
                      };
            
            return req;
        }
    }
}
