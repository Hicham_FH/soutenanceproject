﻿using Core.Service;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.ServiceImpl
{
    public class VilleService : IVilleService
    {
        private readonly DataContext _db;
        public VilleService(DataContext db)
        {
            _db = db;
        }
        public IQueryable<object> getVilleData()
        {
            var req = from v in _db.Villes
                      join p in _db.Pays
                      on v.PaysId equals p.Id
                      select new
                      {
                          id = v.Id,
                          libelle = v.Libelle,
                          pays = p.Libelle,
                          nbrClient = v.Clients.Count()
                      };

            return req;
        }
    }
}
