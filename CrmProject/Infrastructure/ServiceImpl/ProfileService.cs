﻿using Core.Entities;
using Core.Service;
using Infrastructure.Data;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.ServiceImpl
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly DataContext _db;
        public ProfileService(UserManager<ApplicationUser> userManager, DataContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        public async Task<Object> GetUserInfoById(int userID)
        {
            var user = await _userManager.FindByIdAsync(userID.ToString());

             var req = (from ur in _db.UserRoles
                        join r in _db.Roles
                        on ur.RoleId equals r.Id
                        join u in _db.Users
                        on ur.UserId equals u.Id
                        select new
                        {
                            UserID = u.Id,
                            Reference = u.Id +" - " + r.Name,
                            Username = u.UserName,
                            Fullname = u.FullName,
                            Email = u.Email,
                            Role = r.Name,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            Description = u.Description,
                            Tel = u.PhoneNumber,
                            ImageUrl = u.UrlImage
                        }).Where(x => x.UserID == userID);
           
            return req;
        }
    }
}
