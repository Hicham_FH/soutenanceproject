﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class DashboardRepository : IDashboardRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly DataContext _db;

        public DashboardRepository(UserManager<ApplicationUser> userManager, DataContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        public async Task<Object> GetUserInfoById(int userID)
        {
            var user = await _userManager.FindByIdAsync(userID.ToString());

            var req = (from ur in _db.UserRoles
                       join r in _db.Roles
                       on ur.RoleId equals r.Id
                       join u in _db.Users
                       on ur.UserId equals u.Id
                       select new
                       {
                           UserID = u.Id,
                           Username = u.UserName,
                           Fullname = u.FullName,
                           Email = u.Email,
                           Role = r.Name,
                           Tel = u.PhoneNumber
                       }).Where(x => x.UserID == userID);

            return req;
        }


        //===========Get All Users ===========//

        public IEnumerable<Object> getAllUsers()
        {
            var req = (from ur in _db.UserRoles
                       join r in _db.Roles
                       on ur.RoleId equals r.Id
                       join u in _db.Users
                       on ur.UserId equals u.Id
                       select new
                       {
                           id = u.Id,
                           Username = u.UserName,
                           Fullname = u.FullName,
                           Email = u.Email,
                           Role = r.Name,
                           phoneNumber = u.PhoneNumber
                       }).Where(x => x.Role == "Commercial");

            return req.ToList();
        }

        //======== Get All Role ===========//

        public async Task<IEnumerable<ApplicationRole>> getAllRole()
        {
            return await _db.Roles.ToListAsync();
        }



        //========== Get User ==========//

        public async Task<ApplicationUser> getUser(int userID)
        {
            return await _db.Users.FindAsync(userID);
        }

        //======== Delete User ==========//

        public async Task<ApplicationUser> deleteUser(ApplicationUser user)
        {
            _db.Remove(user);
            await _db.SaveChangesAsync();
            return user;
        }

        //======= Get Commercial Number =====//

        public int getCommercialNumber()
        {

            int count = (from ur in _db.UserRoles
                         join u in _db.Users
                         on ur.UserId equals u.Id
                         join r in _db.Roles
                         on ur.RoleId equals r.Id
                         where r.Name == "Commercial"
                         select new
                         {
                             userId = u.Id
                         }).Count();

            return count;
        }

        //======= Get Client Number =====//

        public int getClientNumber()
        {

            int count = _db.Clients.Count();

            return count;
        }
    }

}
