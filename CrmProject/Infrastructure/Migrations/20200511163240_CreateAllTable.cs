﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class CreateAllTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Colonnes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colonnes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EtatInterets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DescriptionKeywords = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EtatInterets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Etats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Etats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pays",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true),
                    Devise = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pays", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Raisons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Raisons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rayons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rayons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Saisons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true),
                    DateDebut = table.Column<DateTime>(nullable: false),
                    DateFin = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Saisons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypePaiements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypePaiements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypeProduits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeProduits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Villes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true),
                    PaysId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Villes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Villes_Pays_PaysId",
                        column: x => x.PaysId,
                        principalTable: "Pays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Niveaus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true),
                    ColonneId = table.Column<int>(nullable: false),
                    RayonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Niveaus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Niveaus_Colonnes_ColonneId",
                        column: x => x.ColonneId,
                        principalTable: "Colonnes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Niveaus_Rayons_RayonId",
                        column: x => x.RayonId,
                        principalTable: "Rayons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttributProduits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    TypeProduitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttributProduits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AttributProduits_TypeProduits_TypeProduitId",
                        column: x => x.TypeProduitId,
                        principalTable: "TypeProduits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Produits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true),
                    Fabricant = table.Column<string>(nullable: true),
                    QR = table.Column<string>(nullable: true),
                    TypeProduitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Produits_TypeProduits_TypeProduitId",
                        column: x => x.TypeProduitId,
                        principalTable: "TypeProduits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(nullable: true),
                    Prenom = table.Column<string>(nullable: true),
                    DateNaissance = table.Column<DateTime>(nullable: false),
                    CIN = table.Column<string>(nullable: true),
                    Adresse = table.Column<string>(nullable: true),
                    Tel1 = table.Column<string>(nullable: true),
                    Tel2 = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Sexe = table.Column<string>(nullable: true),
                    NombreEnfant = table.Column<short>(nullable: false),
                    Fonction = table.Column<string>(nullable: true),
                    DateAjout = table.Column<DateTime>(nullable: false),
                    DateModif = table.Column<DateTime>(nullable: false),
                    Point = table.Column<float>(nullable: false),
                    CA = table.Column<float>(nullable: false),
                    VilleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Clients_Villes_VilleId",
                        column: x => x.VilleId,
                        principalTable: "Villes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Attributs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(nullable: true),
                    Valeur = table.Column<string>(nullable: true),
                    ProduitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attributs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Attributs_Produits_ProduitId",
                        column: x => x.ProduitId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Brouillons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Manque = table.Column<int>(nullable: false),
                    DateCreation = table.Column<DateTime>(nullable: false),
                    ProduitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brouillons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Brouillons_Produits_ProduitId",
                        column: x => x.ProduitId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Lots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QteRestante = table.Column<int>(nullable: false),
                    PrixAchat = table.Column<float>(nullable: false),
                    PrixVente = table.Column<float>(nullable: false),
                    DateExpiration = table.Column<DateTime>(nullable: false),
                    ProduitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lots_Produits_ProduitId",
                        column: x => x.ProduitId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProduitComposers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Qte = table.Column<int>(nullable: false),
                    ProduitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProduitComposers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProduitComposers_Produits_ProduitId",
                        column: x => x.ProduitId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProduitSaisons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ProduitId = table.Column<int>(nullable: false),
                    SaisonId = table.Column<int>(nullable: false),
                    DureeVie = table.Column<int>(nullable: false),
                    MargeRemise = table.Column<float>(nullable: false),
                    NbrJourAvantAlerte = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProduitSaisons", x => new { x.Id, x.ProduitId, x.SaisonId });
                    table.ForeignKey(
                        name: "FK_ProduitSaisons_Produits_ProduitId",
                        column: x => x.ProduitId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProduitSaisons_Saisons_SaisonId",
                        column: x => x.SaisonId,
                        principalTable: "Saisons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Promotions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateDebut = table.Column<DateTime>(nullable: false),
                    DateFin = table.Column<DateTime>(nullable: false),
                    Pourcentage = table.Column<float>(nullable: false),
                    Reduction = table.Column<float>(nullable: false),
                    DateCreation = table.Column<DateTime>(nullable: false),
                    ProduitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Promotions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Promotions_Produits_ProduitId",
                        column: x => x.ProduitId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SousProduits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProduitParentId = table.Column<int>(nullable: true),
                    ProduitEnfantId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SousProduits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SousProduits_Produits_ProduitEnfantId",
                        column: x => x.ProduitEnfantId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SousProduits_Produits_ProduitParentId",
                        column: x => x.ProduitParentId,
                        principalTable: "Produits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientInterets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    EtatInteretId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientInterets", x => new { x.Id, x.ClientId, x.EtatInteretId });
                    table.ForeignKey(
                        name: "FK_ClientInterets_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClientInterets_EtatInterets_EtatInteretId",
                        column: x => x.EtatInteretId,
                        principalTable: "EtatInterets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Commentaires",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Text = table.Column<string>(nullable: true),
                    DateCreation = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commentaires", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Commentaires_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Communications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreation = table.Column<DateTime>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Objectif = table.Column<string>(nullable: true),
                    Commentaire = table.Column<string>(nullable: true),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Communications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Communications_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Remise = table.Column<float>(nullable: false),
                    Avance = table.Column<float>(nullable: false),
                    TotalAPayer = table.Column<float>(nullable: false),
                    TotalRegle = table.Column<float>(nullable: false),
                    IsUrgence = table.Column<bool>(nullable: false),
                    IsRetard = table.Column<bool>(nullable: false),
                    DateLivraisonPrevu = table.Column<DateTime>(nullable: false),
                    Etape = table.Column<string>(nullable: true),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documents_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Enfants",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(nullable: true),
                    Prenom = table.Column<string>(nullable: true),
                    DateNaissance = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enfants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enfants_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EtatClients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    EtatId = table.Column<int>(nullable: false),
                    DateEtatDebut = table.Column<DateTime>(nullable: false),
                    DateEtatFin = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EtatClients", x => new { x.Id, x.ClientId, x.EtatId });
                    table.ForeignKey(
                        name: "FK_EtatClients_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EtatClients_Etats_EtatId",
                        column: x => x.EtatId,
                        principalTable: "Etats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Exceptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exceptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exceptions_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Promesses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreation = table.Column<DateTime>(nullable: false),
                    DateDebut = table.Column<DateTime>(nullable: false),
                    DateFin = table.Column<DateTime>(nullable: false),
                    Pourcentage = table.Column<float>(nullable: false),
                    CAPotentiel = table.Column<float>(nullable: false),
                    Commentaire = table.Column<string>(nullable: true),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Promesses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Promesses_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DepotLots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    LotId = table.Column<int>(nullable: false),
                    ColonneId = table.Column<int>(nullable: false),
                    DateDepot = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepotLots", x => new { x.Id, x.LotId, x.ColonneId });
                    table.ForeignKey(
                        name: "FK_DepotLots_Colonnes_ColonneId",
                        column: x => x.ColonneId,
                        principalTable: "Colonnes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DepotLots_Lots_LotId",
                        column: x => x.LotId,
                        principalTable: "Lots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Depots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QteMax = table.Column<float>(nullable: false),
                    Adresse = table.Column<string>(nullable: true),
                    Lattitude = table.Column<string>(nullable: true),
                    Longtitude = table.Column<string>(nullable: true),
                    VilleId = table.Column<int>(nullable: false),
                    LotId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Depots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Depots_Lots_LotId",
                        column: x => x.LotId,
                        principalTable: "Lots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Depots_Villes_VilleId",
                        column: x => x.VilleId,
                        principalTable: "Villes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LigneDocuments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    DocumentId = table.Column<int>(nullable: false),
                    LotId = table.Column<int>(nullable: false),
                    DateAction = table.Column<DateTime>(nullable: false),
                    PU = table.Column<float>(nullable: false),
                    Action = table.Column<string>(nullable: true),
                    NbrImpression = table.Column<int>(nullable: false),
                    DateImpression = table.Column<DateTime>(nullable: false),
                    CodeLigne = table.Column<string>(nullable: true),
                    Remise = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LigneDocuments", x => new { x.Id, x.DocumentId, x.LotId });
                    table.ForeignKey(
                        name: "FK_LigneDocuments_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LigneDocuments_Lots_LotId",
                        column: x => x.LotId,
                        principalTable: "Lots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Paiements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DatePaiement = table.Column<DateTime>(nullable: false),
                    Montant = table.Column<float>(nullable: false),
                    DateEcheance = table.Column<DateTime>(nullable: false),
                    Timbre = table.Column<bool>(nullable: false),
                    DocumentId = table.Column<int>(nullable: false),
                    TypePaiementId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paiements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Paiements_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Paiements_TypePaiements_TypePaiementId",
                        column: x => x.TypePaiementId,
                        principalTable: "TypePaiements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reclamations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateReclamation = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Niveau = table.Column<string>(nullable: true),
                    DocumentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reclamations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reclamations_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RaisonReclamations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    RaisonId = table.Column<int>(nullable: false),
                    ReclamationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RaisonReclamations", x => new { x.Id, x.RaisonId, x.ReclamationId });
                    table.ForeignKey(
                        name: "FK_RaisonReclamations_Raisons_RaisonId",
                        column: x => x.RaisonId,
                        principalTable: "Raisons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RaisonReclamations_Reclamations_ReclamationId",
                        column: x => x.ReclamationId,
                        principalTable: "Reclamations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AttributProduits_TypeProduitId",
                table: "AttributProduits",
                column: "TypeProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_Attributs_ProduitId",
                table: "Attributs",
                column: "ProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_Brouillons_ProduitId",
                table: "Brouillons",
                column: "ProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientInterets_ClientId",
                table: "ClientInterets",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientInterets_EtatInteretId",
                table: "ClientInterets",
                column: "EtatInteretId");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_VilleId",
                table: "Clients",
                column: "VilleId");

            migrationBuilder.CreateIndex(
                name: "IX_Commentaires_ClientId",
                table: "Commentaires",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Communications_ClientId",
                table: "Communications",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_DepotLots_ColonneId",
                table: "DepotLots",
                column: "ColonneId");

            migrationBuilder.CreateIndex(
                name: "IX_DepotLots_LotId",
                table: "DepotLots",
                column: "LotId");

            migrationBuilder.CreateIndex(
                name: "IX_Depots_LotId",
                table: "Depots",
                column: "LotId");

            migrationBuilder.CreateIndex(
                name: "IX_Depots_VilleId",
                table: "Depots",
                column: "VilleId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_ClientId",
                table: "Documents",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Enfants_ClientId",
                table: "Enfants",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_EtatClients_ClientId",
                table: "EtatClients",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_EtatClients_EtatId",
                table: "EtatClients",
                column: "EtatId");

            migrationBuilder.CreateIndex(
                name: "IX_Exceptions_ClientId",
                table: "Exceptions",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_LigneDocuments_DocumentId",
                table: "LigneDocuments",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_LigneDocuments_LotId",
                table: "LigneDocuments",
                column: "LotId");

            migrationBuilder.CreateIndex(
                name: "IX_Lots_ProduitId",
                table: "Lots",
                column: "ProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_Niveaus_ColonneId",
                table: "Niveaus",
                column: "ColonneId");

            migrationBuilder.CreateIndex(
                name: "IX_Niveaus_RayonId",
                table: "Niveaus",
                column: "RayonId");

            migrationBuilder.CreateIndex(
                name: "IX_Paiements_DocumentId",
                table: "Paiements",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_Paiements_TypePaiementId",
                table: "Paiements",
                column: "TypePaiementId");

            migrationBuilder.CreateIndex(
                name: "IX_ProduitComposers_ProduitId",
                table: "ProduitComposers",
                column: "ProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_Produits_TypeProduitId",
                table: "Produits",
                column: "TypeProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_ProduitSaisons_ProduitId",
                table: "ProduitSaisons",
                column: "ProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_ProduitSaisons_SaisonId",
                table: "ProduitSaisons",
                column: "SaisonId");

            migrationBuilder.CreateIndex(
                name: "IX_Promesses_ClientId",
                table: "Promesses",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Promotions_ProduitId",
                table: "Promotions",
                column: "ProduitId");

            migrationBuilder.CreateIndex(
                name: "IX_RaisonReclamations_RaisonId",
                table: "RaisonReclamations",
                column: "RaisonId");

            migrationBuilder.CreateIndex(
                name: "IX_RaisonReclamations_ReclamationId",
                table: "RaisonReclamations",
                column: "ReclamationId");

            migrationBuilder.CreateIndex(
                name: "IX_Reclamations_DocumentId",
                table: "Reclamations",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_SousProduits_ProduitEnfantId",
                table: "SousProduits",
                column: "ProduitEnfantId");

            migrationBuilder.CreateIndex(
                name: "IX_SousProduits_ProduitParentId",
                table: "SousProduits",
                column: "ProduitParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Villes_PaysId",
                table: "Villes",
                column: "PaysId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttributProduits");

            migrationBuilder.DropTable(
                name: "Attributs");

            migrationBuilder.DropTable(
                name: "Brouillons");

            migrationBuilder.DropTable(
                name: "ClientInterets");

            migrationBuilder.DropTable(
                name: "Commentaires");

            migrationBuilder.DropTable(
                name: "Communications");

            migrationBuilder.DropTable(
                name: "DepotLots");

            migrationBuilder.DropTable(
                name: "Depots");

            migrationBuilder.DropTable(
                name: "Enfants");

            migrationBuilder.DropTable(
                name: "EtatClients");

            migrationBuilder.DropTable(
                name: "Exceptions");

            migrationBuilder.DropTable(
                name: "LigneDocuments");

            migrationBuilder.DropTable(
                name: "Niveaus");

            migrationBuilder.DropTable(
                name: "Paiements");

            migrationBuilder.DropTable(
                name: "ProduitComposers");

            migrationBuilder.DropTable(
                name: "ProduitSaisons");

            migrationBuilder.DropTable(
                name: "Promesses");

            migrationBuilder.DropTable(
                name: "Promotions");

            migrationBuilder.DropTable(
                name: "RaisonReclamations");

            migrationBuilder.DropTable(
                name: "SousProduits");

            migrationBuilder.DropTable(
                name: "EtatInterets");

            migrationBuilder.DropTable(
                name: "Etats");

            migrationBuilder.DropTable(
                name: "Lots");

            migrationBuilder.DropTable(
                name: "Colonnes");

            migrationBuilder.DropTable(
                name: "Rayons");

            migrationBuilder.DropTable(
                name: "TypePaiements");

            migrationBuilder.DropTable(
                name: "Saisons");

            migrationBuilder.DropTable(
                name: "Raisons");

            migrationBuilder.DropTable(
                name: "Reclamations");

            migrationBuilder.DropTable(
                name: "Produits");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "TypeProduits");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "Villes");

            migrationBuilder.DropTable(
                name: "Pays");
        }
    }
}
