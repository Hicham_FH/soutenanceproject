﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class AlterUserClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DescriptionActions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ApplcicationUserId = table.Column<int>(nullable: false),
                    DocumentId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DateCreation = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DescriptionActions", x => new { x.Id, x.ApplcicationUserId, x.DocumentId });
                    table.ForeignKey(
                        name: "FK_DescriptionActions_AspNetUsers_ApplcicationUserId",
                        column: x => x.ApplcicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DescriptionActions_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DescriptionActions_ApplcicationUserId",
                table: "DescriptionActions",
                column: "ApplcicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DescriptionActions_DocumentId",
                table: "DescriptionActions",
                column: "DocumentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DescriptionActions");
        }
    }
}
