﻿using Core.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data
{
    public class DataContext : IdentityDbContext<ApplicationUser , ApplicationRole , int>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }


        //========== OnModelCreating ========//

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);

            //======= Client Interet ====//

            builder.Entity<ClientInteret>().HasKey(c => new { c.Id, c.ClientId, c.EtatInteretId });

            //====== Etat Client ========//

            builder.Entity<EtatClient>().HasKey(e => new { e.Id, e.ClientId, e.EtatId });

            //===== Ligne Document ======//

            builder.Entity<LigneDocument>().HasKey(l => new { l.Id, l.DocumentId, l.LotId });

            //===== Produit Saison ========//

            builder.Entity<ProduitSaison>().HasKey(p => new { p.Id, p.ProduitId, p.SaisonId });

            //===== Depot Lot ====//

            builder.Entity<DepotLot>().HasKey(d => new { d.Id, d.LotId, d.ColonneId });

            //==== Raison Recaamation =====//

            builder.Entity<RaisonReclamation>().HasKey(r => new { r.Id, r.RaisonId, r.ReclamationId });

            //==== Description Action =======//

            builder.Entity<DescriptionAction>().HasKey(d => new { d.Id, d.ApplcicationUserId, d.DocumentId });

            


           // builder.Ignore<DescriptionAction>();



        }




        //========= DBset ===========//

        public DbSet<Client> Clients { get; set; }

        public DbSet<Promesse> Promesses { get; set; }

        public DbSet<Commentaire> Commentaires { get; set; }

        public DbSet<Communication> Communications { get; set; }

        public DbSet<Enfant> Enfants { get; set; }

        public DbSet<Core.Entities.Exception> Exceptions { get; set; }

        public DbSet<Ville> Villes { get; set; }

        public DbSet<Pays> Pays { get; set; }

        public DbSet<Depot> Depots { get; set; }

        public DbSet<EtatInteret> EtatInterets { get; set; }

        public DbSet<ClientInteret> ClientInterets { get; set; }

        public DbSet<Etat> Etats { get; set; }

        public DbSet<EtatClient> EtatClients { get; set; }

        public DbSet<Document> Documents { get; set; }

        public DbSet<Reclamation> Reclamations { get; set; }

        public DbSet<Raison> Raisons { get; set; }

        public DbSet<Paiement> Paiements { get; set; }

        public DbSet<TypePaiement> TypePaiements { get; set; }

        public DbSet<Lot> Lots { get; set; }

        public DbSet<LigneDocument> LigneDocuments { get; set; }

        public DbSet<Colonne> Colonnes { get; set; }

        public DbSet<DepotLot> DepotLots { get; set; }

        public DbSet<Niveau> Niveaus { get; set; }

        public DbSet<Rayon> Rayons { get; set; }

        public DbSet<Produit> Produits { get; set; }

        public DbSet<Attribut> Attributs { get; set; }

        public DbSet<TypeProduit> TypeProduits { get; set; }

        public DbSet<Brouillon> Brouillons { get; set; }

        public DbSet<Promotion> Promotions { get; set; }

        public DbSet<Saison> Saisons { get; set; }

        public DbSet<ProduitSaison> ProduitSaisons { get; set; }

        public DbSet<AttributProduit> AttributProduits { get; set; }

        public DbSet<DescriptionAction> DescriptionActions { get; set; }

        public DbSet<ProduitComposer> ProduitComposers { get; set; }

        public DbSet<SousProduits> SousProduits { get; set; }

        public DbSet<RaisonReclamation> RaisonReclamations { get; set; }


    }
}
