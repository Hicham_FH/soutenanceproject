import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from "./app.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { AppRoutingModule } from "./app-routing.module";
import { ComponentsModule } from "./components/components.module";
import { AuthenticateComponent } from './Authenticate/authenticate/authenticate.component';
import { LoginComponent } from './Authenticate/authenticate/login/login.component';
import { AuthenticateService } from './shared/service/authenticate.service';
import { AuthenticateInterceptor } from './shared/interceptor/authenticate.interceptor';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  imports: [

    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    MatDialogModule
  ],
  declarations: [AppComponent, AdminLayoutComponent, AuthLayoutComponent, AuthenticateComponent , LoginComponent],
  providers: [AuthenticateService , {
    provide: HTTP_INTERCEPTORS,
    useClass : AuthenticateInterceptor ,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {}
