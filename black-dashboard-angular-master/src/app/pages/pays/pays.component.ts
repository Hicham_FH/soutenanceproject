import { Component, OnInit } from '@angular/core';
import { Pays } from 'src/app/shared/model/pays.model';
import { PaysService } from 'src/app/shared/service/pays.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { ProfileComponent } from '../profile/profile.component';
import { PaysDetailsComponent } from './pays-details/pays-details.component';


@Component({
  selector: 'app-pays',
  templateUrl: './pays.component.html',
  styleUrls: ['./pays.component.scss']
})
export class PaysComponent implements OnInit {

  paysData :  []

  constructor(private paysService : PaysService , private dialog : MatDialog ) { }

  ngOnInit(): void {

    this.paysService.getAllPays().subscribe(

      succ => {
        this.paysData = succ as []
        console.log(this.paysData)
      },
      err => {
        console.log(err);
      }

    );

  }

  test() {
    const config = new MatDialogConfig();
    config.width = "65%";
    config.autoFocus = true;
    config.disableClose = true;
    this.dialog.open(PaysDetailsComponent , config);
  }

}
