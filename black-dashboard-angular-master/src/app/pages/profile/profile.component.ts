import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/shared/service/profile.service';
import { Profil } from 'src/app/shared/model/profil.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private profileService : ProfileService) { }

  ProfileData : []


  ngOnInit(): void {
    this.profileService.getProfileData().subscribe(

      (succ : any) => {

        this.ProfileData = succ as []
        console.log(this.ProfileData)

      },
      err => {
        console.log(err);
      }

    );
  }

}
