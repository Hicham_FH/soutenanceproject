export class Profil {
  username : string;
  fullname : string;
  email : string;
  tel : string;
  role : string;
}
