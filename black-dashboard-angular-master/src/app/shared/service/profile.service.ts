import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http : HttpClient) { }

  //======= Start Get Profile Information (User Data) =====//

  getProfileData() {
    return this.http.get(environment.baseURL + 'Profile')
  }

  //======= End Get Profile Information (User Data) =====//




}
