import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  constructor(private http : HttpClient) { }

  /*===== Start Login ===== */

  login(form) {
    return this.http.post(environment.baseURL + "Authenticate/Login" , form);
  }

  /*===== End Login ===== */


// ======= Start Role Decrypre ===//

roleIsCorrect(allowedRole){
  let isCorrect = false;
  const token = sessionStorage.getItem('token');
  let payLoad = JSON.parse(window.atob(token.split('.')[1]));
  let userRole = payLoad.role;
  allowedRole.forEach(element => {
        if(userRole == element) {
          isCorrect = true;
          return false;
        }
  });
  return isCorrect;

}

// ======= End Role Decrypre ===//


}
