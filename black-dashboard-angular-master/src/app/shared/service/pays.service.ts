import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaysService {

  constructor(private http : HttpClient) { }

  /*======== Start Get All Pays ===== */

  getAllPays() {
    return this.http.get(environment.baseURL + "Pays" );
  }

  /*======== End Get All Pays ===== */


}
