import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticateService } from '../service/authenticate.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateGuard implements CanActivate {

  constructor(private router :Router , private userService : AuthenticateService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):boolean {
      if(sessionStorage.getItem('token') != null) {

        let roles = next.data['AccessRole'] as Array<string>;

        if(roles) {

          if(this.userService.roleIsCorrect(roles)) return true;
          else{
            this.router.navigateByUrl('/home/forbidden');
          // console.log('Error from guard')
            return false;
          }
        }

        return true;
      }
      else{
        this.router.navigateByUrl('/')
        return true;
      }

  }

}

