import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";


import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LoginComponent } from './login/login.component';
import { AuthenticateLayoutRoutes } from './authenticate-layout.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthenticateLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
  ],
  declarations: [
    // RtlComponent
  ]
})
export class AuthenticateLayoutModule {}
