import { Routes } from "@angular/router";


import { LoginComponent } from './login/login.component';
// import { RtlComponent } from "../../pages/rtl/rtl.component";

export const AuthenticateLayoutRoutes: Routes = [

  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },

  {
    path: 'login',
    component: LoginComponent,
    data : {
      title : 'Login Page'
    }
  },

  // { path: "rtl", component: RtlComponent }
];
