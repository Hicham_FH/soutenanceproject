import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/model/user.model';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthenticateService } from 'src/app/shared/service/authenticate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private toastr : ToastrService , private authenticateService : AuthenticateService , private router : Router) { }

  userForm : User = {
    username : '',
    password : '',
  }

  ngOnInit(): void {
    if(sessionStorage.getItem('token') != null )
         this.router.navigateByUrl('/dashboard');
  }





  /*======= Start Login ========= */

  onSubmit(form : NgForm) {
    this.authenticateService.login(form.value).subscribe(
      (succ:any) => {
        sessionStorage.setItem('token',succ.token);
        this.router.navigateByUrl('/home');
    },
    err  => {
      if(err.status == 401)
      console.log("hh")
        // this.toastr.error("Username Or Password Not Correct !!" , "Authenticate");
    }

    )
  }

  /*======= End Login ========= */





}
