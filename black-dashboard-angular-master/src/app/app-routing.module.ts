import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { LoginComponent } from './Authenticate/authenticate/login/login.component';
import { AuthenticateComponent } from './Authenticate/authenticate/authenticate.component';
import { AuthenticateGuard } from './shared/guard/authenticate.guard';

const routes: Routes = [
  {
    path: "",
    redirectTo: "authenticate",
    pathMatch: "full"
  },
  {
    path: "",
    component: AdminLayoutComponent,
    children: [
      {
        path: "",
        loadChildren:
          "./layouts/admin-layout/admin-layout.module#AdminLayoutModule"
      }
    ],
    canActivate :[AuthenticateGuard]

  }, {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './layouts/auth-layout/auth-layout.module#AuthLayoutModule'
      }
    ]
  },

  {
    path: 'authenticate',
    component: AuthenticateComponent,
    children: [
      {
        path: '',
        loadChildren: './Authenticate/authenticate/authenticate-layout.module#AuthenticateLayoutModule'
      }
    ]
  },

  {
    path: "**",
    redirectTo: "dashboard"
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
