import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserRoles } from '../models/user-roles.model';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router : Router , private userService : UserService) { }

  profileData;

  ngOnInit(): void {

    this.userService.getProfileInfo().subscribe(
      res => {
        this.profileData = res;
        console.log(this.profileData)
    },
    err => {}
    )

  }


  logOut()
  {
    sessionStorage.removeItem('token');
    this.router.navigateByUrl('');
  }

}
