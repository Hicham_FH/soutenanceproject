import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/service/user.service';
import { User } from 'src/app/shared/models/user.model';
import { NgForm } from '@angular/forms';
import { Role } from 'src/app/shared/models/role.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-commercial',
  templateUrl: './add-commercial.component.html',
  styles: []
})
export class AddCommercialComponent implements OnInit {

  constructor(private userService : UserService , private toastr : ToastrService) { }

  userData : User = {
        id : 0,
        username : '',
        fullname : '',
        email : '',
        role : '',
        phoneNumber : '',
        password : '',
        firstName: '',
        lastName: '',
        adress: '',
        zipCode: 0,
        description: '',
        grade: '',
        urlImage: ''
  }

  roleList : Role [];

  ngOnInit(): void {

    //=======Get All User Role =======//

    this.userService.getAllRole().subscribe(
      res =>{
        this.roleList = res as Role[]
      },
      err =>{
        console.log(err)
      }
    )

  }



  addUser(form : NgForm) {
    this.userService.addUser(form.value).subscribe(
      res =>{
        this.toastr.success("Ajout Avec Succes" , "Nouveau Utilisateur");
      },
      err =>{
        this.toastr.error("erreur");
      }
    )
  }

}
