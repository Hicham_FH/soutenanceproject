import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/service/user.service';
import { AddUserComponent } from '../add-user.component';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.css']
})
export class DetailUserComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data,
  public dialogRef : MatDialogRef<AddUserComponent>,
  private userService : UserService ,
 private toastr : ToastrService

 ) { }

 userData : User = {
  id : 0,
  username : '',
  fullname : '',
  email : '',
  role : '',
  phoneNumber : '',
  password : '',
  firstName: '',
  lastName: '',
  adress: '',
  zipCode: 0,
  description: '',
  grade: '',
  urlImage: ''
}


  ngOnInit(): void {


    if(this.data.userIndex == null ) {
      this.userData = {
        id : 0,
        username : '',
        fullname : '',
        email : '',
        role : '',
        phoneNumber : '',
        password : '',
        firstName: '',
        lastName: '',
        adress: '',
        zipCode: 0,
        description: '',
        grade: '',
        urlImage: ''

      }
    } else {
      this.userData = this.userService.userList[this.data.userIndex];
    }

    console.log(this.userData);

  }

}
