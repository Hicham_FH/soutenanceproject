import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'src/app/shared/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/shared/models/user.model';
import { Role } from 'src/app/shared/models/role.model';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog : MatDialogRef<UpdateUserComponent>,
    public userService : UserService,
    private toastr : ToastrService
  ) { }

  userData : User = {
    id : 0,
        username : '',
        fullname : '',
        email : '',
        role : '',
        phoneNumber : '',
        password : '',
        firstName: '',
        lastName: '',
        adress: '',
        zipCode: 0,
        description: '',
        grade: '',
        urlImage: ''
 }
 roleList : Role [];
  ngOnInit(): void {

    if(this.data.index == null ) {
      this.userData = {
        id : 0,
        username : '',
        fullname : '',
        email : '',
        role : '',
        phoneNumber : '',
        password : '',
        firstName: '',
        lastName: '',
        adress: '',
        zipCode: 0,
        description: '',
        grade: '',
        urlImage: ''

      }
    } else {
      this.userData = this.userService.userList[this.data.index];
    }
    //=======Get All User Role =======//

    this.userService.getAllRole().subscribe(
      res =>{
        this.roleList = res as Role[]
      },
      err =>{
        console.log(err)
      }
    )
    //console.log(this.userData);

  }

  //========= Update User ======//

  updateUser(form) {
    console.log(this.data.id);
    this.userService.updateUser(this.userData.id, form.value).subscribe(
      res => {
        this.toastr.warning("Modification Avec Succes" , "Modifier User");
       // this.dialog.close();
      },
      err => {
        console.log(err);
      }
    ) 
  }
}
