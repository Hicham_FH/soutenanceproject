import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddCommercialComponent } from './add-commercial/add-commercial.component';
import { UserService } from 'src/app/shared/service/user.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import { DetailPaysComponent } from '../pays/detail-pays/detail-pays.component';
import { DetailUserComponent } from './detail-user/detail-user.component';
import { User } from 'src/app/shared/models/user.model';
import { UserRoles } from '../models/user-roles.model';
import { UpdateUserComponent } from './update-user/update-user.component';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(public userService : UserService , public dialog : MatDialog ) { }

  commercialData;

  ngOnInit(): void {

    /*this.userService.getAllUsers().subscribe(
      res =>{
        this.commercialData = res;
        console.log(this.commercialData)
      },
      err => {
        console.log(err);
      }
    )
*/

this.getData();
  }

  /*======== Start Get Data =========== */

  getData() {
    this.userService.getAllUsers().subscribe(
      res=> {
        this.userService.userList = res as User[]

      },
      err=>{
        console.log(err);
      }
    )
  }

  /*======== EndGet Data =========== */

  addUser() {
    const config = new MatDialogConfig();
    config.width = "50%";
    config.height = "650px";
    config.autoFocus = true;
    config.disableClose = true;

    this.dialog.open(AddCommercialComponent , config)
  }

  delete() {


    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your imaginary file has been deleted.',
          'success'
        )
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })



  }


  /*====== STart Details User ======= */

  detailUser(userIndex) {
    const config = new MatDialogConfig();
    config.width = "50%";
    config.height = "600px";
    config.autoFocus = true;
    config.disableClose = true;
    config.data = {userIndex}
    config.position = {

      top: '1%',
      left: '10%'

  }
    this.dialog.open(DetailUserComponent , config)
  }

  /*====== End Details User ======= */

  /*========Start Update User */


  updateUser(index , userID) {
    const config = new MatDialogConfig();
    config.width = "50%";
    config.height = "450px";
    config.autoFocus = true;
    config.disableClose = true;
    config.data = {index , userID}

    this.dialog.open(UpdateUserComponent , config);
  }

  /*========End Update User */


}
