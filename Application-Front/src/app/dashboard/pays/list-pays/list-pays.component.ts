import { Component, OnInit, OnDestroy } from '@angular/core';
import { PaysService } from 'src/app/shared/service/pays/pays.service';
import { Pays } from 'src/app/shared/models/pays/pays.model';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddPaysComponent } from '../add-pays/add-pays.component';

import Swal from 'sweetalert2'
import { DetailPaysComponent } from '../detail-pays/detail-pays.component';
import { UpdatePaysComponent } from '../update-pays/update-pays.component';

@Component({
  selector: 'app-list-pays',
  templateUrl: './list-pays.component.html',
  styleUrls: ['./list-pays.component.css']
})
export class ListPaysComponent implements OnInit , OnDestroy {

  constructor(public paysService : PaysService , private dialog : MatDialog) { }

  paysData : Pays [];

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<Pays> = new Subject();



  ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      paging : true
    };

    this.getData();



  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  //=======Get Data ============//

  getData() {
    this.paysService.getAllPays().subscribe(
      res=> {
        this.paysService.PaysList = res as Pays[]
        this.dtTrigger.next();
      },
      err=>{
        console.log(err);
      }
    )
  }

  //======== Add Pays ========//

  addPays() {
    const config = new MatDialogConfig();
    config.width = "50%";
    config.height = "450px";
    config.autoFocus = true;
    config.disableClose = true;
    config.position = {

      top: '2%',
      left: '10%'

  }

    this.dialog.open(AddPaysComponent , config);

  }

  //====== Delete Pays ========//

  delete(id) {


    Swal.fire({
      title: 'Voulez Vous Vraiment Supprimer?',
      text: 'Oui ?? / Non ?? ',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui , Supprimer !',
      cancelButtonText: 'Non '
    }).then((result) => {
      if (result.value) {

        this.paysService.deletePays(id).subscribe(
          res => {
            Swal.fire(
              'Suppression Effectué',
              'Votre Enregistrement A Eté Bien Supprimer',
              'success'
            )

            this.getData();

          }
        )

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Annulation',
          'Vous Avez Annuler La Suppression',
          'error'
        )
      }
    })
  }

  //====== Details Pays =========//

  detail(paysIndex) {
    const config = new MatDialogConfig();
    config.width = "50%";
    config.height = "480px";
    config.autoFocus = true;
    config.disableClose = true;
    config.data = {paysIndex};
    config.position = {

      top: '2%',
      left: '10%'

  }

    this.dialog.open(DetailPaysComponent , config);
  }


  //====== Edit Pays =====//

  edit(index , PaysID) {

    const config = new MatDialogConfig();
    config.width = "50%";
    config.height = "450px";
    config.autoFocus = true;
    config.disableClose = true;
    config.data = {index , PaysID}

    this.dialog.open(UpdatePaysComponent , config);

  }


}
