import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PaysService } from 'src/app/shared/service/pays/pays.service';
import { Pays } from 'src/app/shared/models/pays/pays.model';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-update-pays',
  templateUrl: './update-pays.component.html',
  styleUrls: ['./update-pays.component.css']
})
export class UpdatePaysComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog : MatDialogRef<UpdatePaysComponent>,
    public paysService : PaysService,
    private toastr : ToastrService
  ) { }

  paysData : Pays = {
    id : 0,
    libelle : '',
    devise : '',
    nbrClient : 0,
    nbrVille : 0

  }

  ngOnInit(): void {



    if(this.data.index == null ) {
      this.paysData = {
        id : 0,
        libelle : '',
        devise : '',
        nbrClient : 0,
        nbrVille : 0

      }
    } else {
      this.paysData = this.paysService.PaysList[this.data.index];
    }

   // console.log(this.paysData);


  }

  //========= Update Pays ======//

  updatePays(form) {
      this.paysService.updatePays(this.data.PaysID , form.value).subscribe(
        res => {
          this.toastr.warning("Modification Avec Succes" , "Modifier Pays");
         // this.dialog.close();
        },
        err => {
          console.log(err);
        }
      )
  }

}
