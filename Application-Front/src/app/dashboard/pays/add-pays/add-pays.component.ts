import { Component, OnInit, Inject } from '@angular/core';
import { PaysService } from 'src/app/shared/service/pays/pays.service';
import { Pays } from 'src/app/shared/models/pays/pays.model';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-pays',
  templateUrl: './add-pays.component.html',
  styleUrls: ['./add-pays.component.css']
})
export class AddPaysComponent implements OnInit {

  constructor( @Inject(MAT_DIALOG_DATA) public data,
              public dialogRef : MatDialogRef<AddPaysComponent>,
              private paysService : PaysService ,
             private toastr : ToastrService

             ) { }

  paysData : Pays = {
    id : 0,
    libelle : '' ,
    devise : '',
    nbrClient : 0,
    nbrVille : 0
  }

  ngOnInit(): void {
   // this.resetInfo();
  }

  //====== Reset Info ====//

  resetInfo(form? : NgForm) {
    form.resetForm();
    this.paysData = {
      id : 0,
      libelle : '' ,
      devise : '',
      nbrClient : 0,
      nbrVille : 0
    }
  }

  //======= Add User Function ====//

  addUser(form : NgForm) {
    this.paysService.postPays(form.value).subscribe(
      res=>{
        this.toastr.success("Ajout Avec Success !! " , "Nouveau Pays");
        this.paysService.PaysList.push(form.value);
        this.resetInfo(form);

      },
      err=>{
        this.toastr.error("Un Erreur !! Veuillez Saisir Les Information Correct" , "Erreur De Saisi");
      }
    )
  }

  //======= Add User & Close ========//

  addUserRetour(form : NgForm) {

        this.toastr.success("Ajout Avec Success !! " , "Nouveau Pays");
        this.paysService.PaysList.push(form.value);
        this.dialogRef.close();


  }

}
