import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddPaysComponent } from '../add-pays/add-pays.component';
import { PaysService } from 'src/app/shared/service/pays/pays.service';
import { ToastrService } from 'ngx-toastr';
import { Pays } from 'src/app/shared/models/pays/pays.model';

@Component({
  selector: 'app-detail-pays',
  templateUrl: './detail-pays.component.html',
  styleUrls: ['./detail-pays.component.css']
})
export class DetailPaysComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data,
              public dialogRef : MatDialogRef<AddPaysComponent>,
              private paysService : PaysService ,
             private toastr : ToastrService
            
             ) { }

  paysData : Pays =  {
    id : 0,
    libelle : '',
    devise : '',
    nbrClient : 0,
    nbrVille : 0
  };

  ngOnInit(): void {
   
    if(this.data.paysIndex == null ) {
      this.paysData = {
        id : 0,
        libelle : '',
        devise : '',
        nbrClient : 0,
        nbrVille : 0

      }
    } else {
      this.paysData = this.paysService.PaysList[this.data.paysIndex];
    }

    console.log(this.paysData);

  }

  

}
