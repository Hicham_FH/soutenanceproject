import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileData;
  constructor(private userService : UserService) { }

  ngOnInit(): void {

    //====File JS===//
  // $.getScript('src/assets_v2/js/main.js');

    this.userService.getProfileInfo().subscribe(
      res => {
        this.profileData = res;
    },
    err => {}
    )
  }


}
