import { Component, OnInit } from '@angular/core';
import { Ville } from 'src/app/shared/models/ville/ville.model';
import { Pays } from 'src/app/shared/models/pays/pays.model';
import { PaysService } from 'src/app/shared/service/pays/pays.service';
import { VilleService } from 'src/app/shared/service/ville/ville.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-ville',
  templateUrl: './add-ville.component.html',
  styleUrls: ['./add-ville.component.css']
})
export class AddVilleComponent implements OnInit {

  constructor(private paysService : PaysService , private villeService : VilleService) { }

  villeData : Ville = {
    id : 0,
  libelle : "",
  pays : "",
  paysId : 0,
  nbrClient : 0
  }

  /*===== Start Pays List ===== */

  paysList : Pays[];
  /*===== End Pays List ===== */

  ngOnInit(): void {

     //=======Get All Pays  =======//

     this.paysService.getAllPays().subscribe(
      res =>{
        this.paysList = res as Pays[]
      },
      err =>{
        console.log(err)
      }
    )


  }

  /*====== Start Add Ville ====== */

  addVille(form : NgForm) {
    this.villeService.postVille(form.value).subscribe(
      succ => {
        console.log(form.value)
      },
      err => {
        console.log(err);
      }
    )
  }

  /*====== End Add Ville ====== */



}
