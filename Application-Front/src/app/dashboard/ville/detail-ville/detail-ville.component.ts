import { Component, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { VilleService } from 'src/app/shared/service/ville/ville.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Ville } from 'src/app/shared/models/ville/ville.model';

@Component({
  selector: 'app-detail-ville',
  templateUrl: './detail-ville.component.html',
  styleUrls: ['./detail-ville.component.css']
})
export class DetailVilleComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data,
  public dialogRef : MatDialogRef<DetailVilleComponent>,
  private villeService : VilleService ,
 private toastr : ToastrService

 ) { }


 villeData : Ville = {
  id : 0,
libelle : "",
pays : "",
paysId : 0,
nbrClient : 0
}

  ngOnInit(): void {

    if(this.data.villeIndex == null ) {

  this.villeData = {
      id : 0,
    libelle : "",
    pays : "",
    paysId : 0,
    nbrClient : 0
  }
    } else {
      this.villeData = this.villeService.villeList[this.data.villeIndex];
    }

    console.log(this.villeData);
  }



}
