import { Component, OnInit } from '@angular/core';
import { Ville } from 'src/app/shared/models/ville/ville.model';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { VilleService } from 'src/app/shared/service/ville/ville.service';
import { Subject } from 'rxjs';
import { AddVilleComponent } from './add-ville/add-ville.component';
import { DetailVilleComponent } from './detail-ville/detail-ville.component';

@Component({
  selector: 'app-ville',
  templateUrl: './ville.component.html',
  styleUrls: ['./ville.component.css']
})
export class VilleComponent implements OnInit {

   constructor(public villeService : VilleService , private dialog : MatDialog) { }

  villeData : Ville [];

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<Ville> = new Subject();



  ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      paging : true
    };

    this.getData();



  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  //=======Get Data ============//

  getData() {
    this.villeService.getAllVille().subscribe(
      res=> {
        this.villeService.villeList = res as Ville[]
        this.dtTrigger.next();
        console.log(this.villeService.villeList);
      },
      err=>{
        console.log(err);
      }
    )
  }

  //======== Add Pays ========//

  addVille() {
    const config = new MatDialogConfig();
    config.width = "50%";
    config.height = "450px";
    config.autoFocus = true;
    config.disableClose = true;

    this.dialog.open(AddVilleComponent , config);

  }


  /*======= Detail Ville========== */

  detail(villeIndex) {
    const config = new MatDialogConfig();
    config.width = "55%";
    config.height = "500px";
    config.autoFocus = true;
    config.disableClose = true;
    config.data = {villeIndex}
    config.position = {

      top: '2%',
      left: '10%'

  }
    this.dialog.open(DetailVilleComponent , config)
  }


}
