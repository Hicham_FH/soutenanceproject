import { Component, OnInit } from '@angular/core';
import { StatistiqueService } from 'src/app/shared/service/statistique/statistique.service';
import { UserService } from 'src/app/shared/service/user.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-statistique',
  templateUrl: './statistique.component.html',
  styleUrls: ['./statistique.component.css']
})
export class StatistiqueComponent implements OnInit {

  constructor(private statistiqueService : StatistiqueService , private userService : UserService) { }

  commercialNumber : number;
  clientNumbre : number;
  userData;

  ngOnInit(): void {

    /*==== Get Commercial Nombre ===== */

    this.statistiqueService.NombreCommercial().subscribe(
      res => {
        this.commercialNumber = res as number;
        console.log(this.commercialNumber);
      },
      err => {
        console.log(err);
      }
    )

    /*===== Get Client Nombre ====== */

    this.statistiqueService.NombreClient().subscribe(
      res => {
        this.clientNumbre = res as number;
      },
      err=> {
        console.log(err);
      }

    )

    /*===== Get User Data ===== */

    this.userService.getProfileInfo().subscribe(
      res => {
        this.userData = res
        console.log(this.userData)
      },
      err => {
        console.log(err)
      }
    )
  }

}
