import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/service/user.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  UserForm : User = {
    id : 0,
        username : '',
        fullname : '',
        email : '',
        role : '',
        phoneNumber : '',
        password : '',
        firstName: '',
        lastName: '',
        adress: '',
        zipCode: 0,
        description: '',
        grade: '',
        urlImage: ''
  }

  constructor(private userService : UserService , private router : Router) { }

  ngOnInit(): void {
    if(sessionStorage.getItem('token') != null )
         this.router.navigateByUrl('/home');
  }

  login(form : NgForm) {
    this.userService.login(form.value).subscribe(
      (succ:any) => {
        sessionStorage.setItem('token',succ.token);
        this.router.navigateByUrl('/home/statistique');
    },
    err  => {
      if(err.status == 401)
      console.log("hh")
        // this.toastr.error("Username Or Password Not Correct !!" , "Authenticate");
    }

    )
  }

}
