import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';



import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserInterceptor } from './Authenticate/interceptor/user.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import { AddCommercialComponent } from './dashboard/add-user/add-commercial/add-commercial.component';
import { UserService } from './shared/service/user.service';
import { ToastrModule } from 'ngx-toastr';

import { DataTablesModule } from 'angular-datatables';
import { DetailPaysComponent } from './dashboard/pays/detail-pays/detail-pays.component';
import { AddPaysComponent } from './dashboard/pays/add-pays/add-pays.component';
import { ClientComponent } from './dashboard/client/client.component';
import { AddClientComponent } from './dashboard/client/add-client/add-client.component';
import { UpdateClientComponent } from './dashboard/client/update-client/update-client.component';
import { DetailsClientComponent } from './dashboard/client/details-client/details-client.component';
import { DetailUserComponent } from './dashboard/add-user/detail-user/detail-user.component';
import { UpdateUserComponent } from './dashboard/add-user/update-user/update-user.component';
import { VilleComponent } from './dashboard/ville/ville.component';
import { AddVilleComponent } from './dashboard/ville/add-ville/add-ville.component';
import { UpdateVilleComponent } from './dashboard/ville/update-ville/update-ville.component';
import { DetailVilleComponent } from './dashboard/ville/detail-ville/detail-ville.component';
import { StatistiqueComponent } from './dashboard/statistique/statistique.component';







@NgModule({
  declarations: [
    AppComponent,
    routingComponent,
    AddCommercialComponent,
    DetailPaysComponent,
    ClientComponent,
    AddClientComponent,
    UpdateClientComponent,
    DetailsClientComponent,
    DetailUserComponent,
    UpdateUserComponent,
    VilleComponent,
    AddVilleComponent,
    UpdateVilleComponent,
    DetailVilleComponent,
    StatistiqueComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    ToastrModule.forRoot(),
    DataTablesModule


  ],
  providers: [UserService , {
    provide: HTTP_INTERCEPTORS,
    useClass : UserInterceptor ,
    multi : true
  }],
  bootstrap: [AppComponent],
  entryComponents : [AddPaysComponent , DetailPaysComponent]
})
export class AppModule { }
