export class User {
    id : number;
    username : string;
    fullname : string;
    email : string;
    role : string;
    phoneNumber : string;
    password : string;
    firstName: string;
    lastName: string;
    adress: string;
    zipCode: number;
    description: string;
    grade: string;
    urlImage: string;
}
