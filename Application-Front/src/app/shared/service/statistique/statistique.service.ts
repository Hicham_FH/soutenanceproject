import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StatistiqueService {

  constructor(private http : HttpClient) { }

  //======== Nombre Commercial ======//

  NombreCommercial() {
    return this.http.get(environment.baseURL + 'Dashboard/numberCommercial');
  }
  NombreClient() {
    return this.http.get(environment.baseURL + 'Dashboard/numberClient');
  }
}
