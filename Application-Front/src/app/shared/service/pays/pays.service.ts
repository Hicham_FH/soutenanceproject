import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pays } from '../../models/pays/pays.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaysService {

  constructor(private http : HttpClient) { }

  PaysList : Pays [];


  //=======Get All Pays =====//

  getAllPays() {
    return this.http.get(environment.baseURL + "Pays");
  }


  //===== Post Pays ====//

  postPays(form) {
    return this.http.post(environment.baseURL + "Pays" , form);
  }

  //======= Delete Pays =====//

  deletePays(id) {
    return this.http.delete(environment.baseURL + "Pays/" +id);
  }


  //===== Update Pays =====//

  updatePays(id , pays) {
    return this.http.put(environment.baseURL + "Pays/" + id , pays);
  }






}
