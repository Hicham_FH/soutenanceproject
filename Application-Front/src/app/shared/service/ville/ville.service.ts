import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Ville } from '../../models/ville/ville.model';

@Injectable({
  providedIn: 'root'
})
export class VilleService {

  constructor(private http : HttpClient) { }

  villeList : Ville [];


  //=======Get All Pays =====//

  getAllVille() {
    return this.http.get(environment.baseURL + "Ville");
  }


  //===== Post Pays ====//

  postVille(form) {
    return this.http.post(environment.baseURL + "Ville" , form);
  }

  //======= Delete Pays =====//

  deleteVille(id) {
    return this.http.delete(environment.baseURL + "Ville/" +id);
  }


  //===== Update Pays =====//

  updateVille(id , pays) {
    return this.http.put(environment.baseURL + "Ville/" + id , pays);
  }



}
