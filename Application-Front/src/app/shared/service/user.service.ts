import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserRoles } from 'src/app/Dashboard/models/user-roles.model';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  userList : User[];

  constructor(private http : HttpClient) { }

  //======Login =====//

  login(form) {
   return this.http.post(environment.baseURL + "Authenticate/Login" , form )
  }

  //====== Add User ========//

  addUser(form) {
    return this.http.post(environment.baseURL + "Authenticate/Register" , form)
  }
//===== Update user =====//

  updateUser(id , user) {
  return this.http.put(environment.baseURL + "user/" + id , user);
  }
  //======= Get All Role =====//

  getAllRole() {
    return this.http.get(environment.baseURL + "Dashboard/GetAllRole");
  }

  //======= Role Decrypre ===//

  roleIsCorrect(allowedRole){
    let isCorrect = false;
    const token = sessionStorage.getItem('token');
    let payLoad = JSON.parse(window.atob(token.split('.')[1]));
    let userRole = payLoad.role;
    allowedRole.forEach(element => {
          if(userRole == element) {
            isCorrect = true;
            return false;
          }
    });
    return isCorrect;

  }


  //========Get User Info ====//
  getProfileInfo() {
    return this.http.get<UserRoles>(environment.baseURL + "Dashboard/GetUserByID" );
  }

//=======Get All User =========//

getAllUsers() {
  return this.http.get(environment.baseURL + "Dashboard/GetAllUsers");
}


}
