import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Authenticate/login/login.component';


import { ProfileComponent } from './Dashboard/profile/profile.component';
import { HomeComponent } from './dashboard/home/home.component';
import { UserGuard } from './Authenticate/guards/user.guard';
import { AddUserComponent } from './dashboard/add-user/add-user.component';
import { ForbiddenComponent } from './dashboard/forbidden/forbidden.component';
import { ListPaysComponent } from './dashboard/pays/list-pays/list-pays.component';
import { AddPaysComponent } from './dashboard/pays/add-pays/add-pays.component';
import { UpdatePaysComponent } from './dashboard/pays/update-pays/update-pays.component';
import { DetailPaysComponent } from './dashboard/pays/detail-pays/detail-pays.component';
import { ClientComponent } from './dashboard/client/client.component';
import { VilleComponent } from './dashboard/ville/ville.component';
import { StatistiqueComponent } from './dashboard/statistique/statistique.component';


const routes: Routes = [
  { path : '' , redirectTo :'/login' , pathMatch : 'full'},
  { path : 'login' , component : LoginComponent},
  {path : 'home' , component : HomeComponent , canActivate :[UserGuard] , children : [
    {path : 'statistique' , component : StatistiqueComponent},
    {path : 'profile' , component : ProfileComponent},
    {path : 'pays' , component : ListPaysComponent},
    {path : 'client' , component : ClientComponent},
    {path : 'ville' , component : VilleComponent},
    {path : 'addCommercial' , component : AddUserComponent , canActivate : [UserGuard] , data : {AccessRole : ['Admin']}},
    {path : 'forbidden' , component : ForbiddenComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [LoginComponent , HomeComponent , ProfileComponent , ForbiddenComponent , AddUserComponent
                              , ListPaysComponent  , AddPaysComponent , UpdatePaysComponent , DetailPaysComponent]
